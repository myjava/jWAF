package com.gitee.seawaf.filters;

import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.gitee.seawaf.InputAttackInfo;
import com.gitee.seawaf.config.WafConfig;
import com.gitee.seawaf.config.mode.InputValidator;
import com.gitee.seawaf.config.mode.InputValidatorPattern;
import com.gitee.seawaf.utils.CacheUtils;
import com.gitee.seawaf.utils.Utils;
/**
 * 将HttpRequest封装为安全请求
 * @author haison
 *
 */
public class WafRequest extends HttpServletRequestWrapper {
	private static Logger logger = Logger.getLogger(WafRequest.class.getCanonicalName());
	
	public WafRequest(HttpServletRequest request) {
		super(request);
	}

	/**
	 * 当通过getParameter获取参数时，系统自动按照规则校验用户的输入
	 * 若匹配到了指定的规则，则执行规则指定的动作，并记录本次匹配
	 * 当ip里面出现的攻击次数超限以后，自动封禁IP
	 */
	public String getParameter(String name){
		InputValidator validator = WafConfig.getInstance().getActivatedMode().getDefence().getInputValidator();
		String url=super.getRequestURL().toString();
		String value = super.getParameter(name);
		String ip=super.getRemoteAddr();
		Pattern globalNamesExcept=validator.getExceptNamesPattern();
		if(value==null||globalNamesExcept!=null && globalNamesExcept.matcher(name).find()){
			//global name except
			return value;
		}else{
			List<InputValidatorPattern> patterns = validator.getPatterns();
			for(InputValidatorPattern pattern:patterns){
				Pattern p1=pattern.getExceptNamesPattern();
				Pattern p2=pattern.getExceptUrlsPattern();
				if(p1!=null && p1.matcher(name).find()||p2!=null && p2.matcher(url).find()){
					//pattern name or URL except
					logger.info("input check ignored because name or url is except");
					continue;
				}
				Matcher matcher = pattern.getExpressionPattern().matcher(value);
				if(matcher.find()){
					logger.warning("ATTACK DETECTED IN NAME["+name+"],FOR INPUT"+value);
					InputAttackInfo attack = new InputAttackInfo(pattern.getName(), url);
					attack.setName(name);
					attack.setValue(value);
					CacheUtils.getInstance().put("attacks",ip+":"+super.getSession().getId(), attack);
					CacheUtils.getInstance().enc("attack.counters", ip);
					int times=CacheUtils.getInstance().getIntValue("attack.counters", ip);
					if(times>100){
						Utils.denyIp(ip, "Too many attack detected");
						logger.warning("ip["+ip+"] added to denied list");
					}
					if(pattern.getAction()==InputValidatorPattern.ACTION_REPLACE){
						do{							
							value = matcher.replaceAll("");
							matcher = pattern.getExpressionPattern().matcher(value);
						}while(matcher.find());
					}
				}else{
					continue;
				}
			}
		}
		return value;
	}

}
